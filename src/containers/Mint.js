import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import styles from './Mint.css';
import { ActionCreators } from '../actions';
import Coin from '../components/Coin';
import Form from '../components/Form';

const generateCoins = (coins, actions) => {
	return coins.map((coin, index) => (
		<Coin
			key={index}
			{...coin}
			onChange={actions.changeCoin}
			onBlur={actions.editCoin}
			/>
	));
}

const Mint = ({state, actions}) => {
	const { coins, value } = state;

	return (
		<div className={styles.mint}>
			<h1 className={styles.header}>Coin Counter</h1>
			<div className={styles.coins}>
				{generateCoins(coins, actions)}
			</div>
			<Form
				value={value}
				onClick={actions.calculate}
				onChange={actions.updateValue}
				/>
		</div>
	);
}

Mint.propTypes = {
	state: React.PropTypes.object,
	actions: React.PropTypes.objectOf(React.PropTypes.func)
};

function mapStateToProps (state) {
	return { state };
}

function mapDispatchToProps (dispatch) {
  return {
		actions: bindActionCreators(ActionCreators, dispatch)
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(Mint);
