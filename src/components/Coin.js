import React, { Component } from 'react';
import styles from './Coin.css';

const renderFace = face => <span className={styles.coin_face}>{face}</span>;
const renderAmount = amount => <span className={styles.coin_amount}>{amount}</span>;
const renderInput = (index, face, onBlur) => (
	<input
		type="number"
		className={styles.coin_face_input}
		defaultValue={face}
		onBlur={evt => onBlur(index, +evt.target.value)}
		autoFocus
		/>
);
const renderChange = (index, onClick) => (
	<span
		className={styles.coin_change}
		onClick={() => onClick(index)}
		>
		Change
	</span>
)

const Coin = ({ index, editing, face, amount, onChange, onBlur }) => {
	const coinFace = !editing ? renderFace(face) : renderInput(index, face, onChange);
	const changeBtn = editing || face === 1 ? null : renderChange(index, onBlur);
	const coinAmount = !amount ? null : renderAmount(amount);

	return (
		<div className={styles.coin}>
			{coinFace}
			{coinAmount}
			{changeBtn}
		</div>
	);
}

Coin.propTypes = {
	index: React.PropTypes.number,
	editing: React.PropTypes.bool,
	face: React.PropTypes.number.isRequired,
	amount: React.PropTypes.number,
	onChange: React.PropTypes.func,
	onBlur: React.PropTypes.func
};

export default Coin;
