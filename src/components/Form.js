import React, { Component } from 'react';
import styles from './Form.css';

const Form = ({ value, onClick, onChange }) => (
	<form
		className={styles.form}
		onSubmit={evt => {
			evt.preventDefault();
			onClick();
		}}
		>
		<input
			className={styles.input}
			type={'number'}
			value={value}
			onChange={evt => onChange(+evt.target.value)}
			/>
		<button className={styles.button}>Calculate</button>
	</form>
);

Form.propTypes = {
	value: React.PropTypes.number.isRequired,
	onClick: React.PropTypes.func.isRequired,
	onChange: React.PropTypes.func.isRequired
};

export default Form;
