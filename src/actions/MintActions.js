import { CALCULATE, UPDATE_VALUE, CHANGE_COIN, EDIT_COIN } from './types';

export function calculate (value) {
	return {
		type: CALCULATE
	};
}

export function updateValue (value) {
	return {
		type: UPDATE_VALUE,
		value
	};
}

export function changeCoin (index, value) {
	return {
		type: CHANGE_COIN,
		index,
		value
	};
}

export function editCoin (index) {
	return {
		type: EDIT_COIN,
		index
	}
}
