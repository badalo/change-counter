export const defaultState = {
	coins: [
		{
			index: 0,
			editing: false,
			face: 25,
			amount: 0
		},
		{
			index: 1,
			editing: false,
			face: 10,
			amount: 0
		},
		{
			index: 2,
			editing: false,
			face: 5,
			amount: 0
		},
		{
			index: 3,
			editing: false,
			face: 1,
			amount: 0
		}
	],
	value: 0
}
