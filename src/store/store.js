import { createStore, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import createLogger from 'redux-logger';
import promise from 'redux-promise';
import MintReducers from '../reducers/MintReducers';
import { defaultState } from './default';

const logger = createLogger();

export default function GetStore (initialState = defaultState) {
	const enhancer = compose(
		applyMiddleware(thunk, promise, logger)
	);

	return createStore(MintReducers, initialState, enhancer);
}
