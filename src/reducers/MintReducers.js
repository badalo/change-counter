import { CALCULATE, UPDATE_VALUE, CHANGE_COIN, EDIT_COIN } from '../actions/types';

export default (state, action) => {
	switch(action.type) {
		case CALCULATE:
			return calculate(state);
		case UPDATE_VALUE:
			return updateValue(state, action.value)
		case CHANGE_COIN:
			return changeCoin(state, action.index, action.value);
		case EDIT_COIN:
			return editCoin(state, action.index);
		default:
			return state;
	}
}

function calculate (state) {
	let remainder = state.value;
	const coins = [].concat(state.coins).sort((a, b) => b.face - a.face);

	coins.forEach(coin => {
		state.coins[coin.index].editing = false;

		if (!remainder || remainder < coin.face) {
			state.coins[coin.index].amount = 0;
			return;
		}

		const amount = parseInt(remainder / coin.face, 10);
		remainder = remainder % coin.face;

		state.coins[coin.index].amount = amount;
	});

	return {
		coins: state.coins,
		value: state.value
	};
}

function updateValue (state, value) {
	return {
		coins: state.coins,
		value: value
	}
}

function changeCoin (state, index, value) {
	return {
		coins: state.coins.map(coin => ({
			index: coin.index,
			editing: false,
			face: coin.index === index ? value : coin.face,
			amount: 0
		})),
		value: 0
	};
}

function editCoin (state, index) {
	return {
		coins: state.coins.map(coin => Object.assign(coin, {
			amount: 0,
			editing: coin.index === index
		})),
		value: 0
	};
}
