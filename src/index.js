import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import Mint from './containers/Mint';
import GetStore from './store/store';

const store = GetStore();

const App = (
	<Provider store={store}>
		<Mint />
	</Provider>
);

render(App, document.getElementById('mint'));
