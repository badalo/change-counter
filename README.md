# Installation
These installation notes, assume you have already installed [Yarn](https://yarnpkg.com/en/docs/install)

On a Terminal window run:

* `mkdir <path/to/folder>`

* `git clone https://badalo@bitbucket.org/badalo/change-counter.git <path/to/folder>`

* `cd <path/to/folder>`

* `yarn install`


# Run DEV server
On a Terminal window run

* `yarn start`

On a browse window open

* `http://localhost:8080/`

# Run tests
On a Terminal window run

* `yarn run test`

# Notes
It was a nice and fun homework to do. Something we have in our daily lives, but, and to be honest, never thought how it was done.

When reviewing the code, I noticed that the approach I had taken was a mix one (stateless and classes components) which is not the recommended way to do it when using React with Redux. So, during yesterday’s flight, I’ve changed the classes components to be stateless ones, which lead me to have to change the created tests and create some more new reducers.

Another thing I've notice, it that the _calculate_ reducer could be improved to optimize the amount of change given. For instance, when asking to calculate the change for 12, using the default coins, it shows 1 coin for 10 and 2 for 1 (total of 3 coins). However, if we change the 5 coin to be 6, the less change possible would be 2 coins of 6, instead of the same result as shown before. This happens because I'm ordering the coins descending.

So the _calculate_ reducer should also check each coin and if the value is divisible by it's face and calculate the total amount of coins and return the combination that uses less coins.
