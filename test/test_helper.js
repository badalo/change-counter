import jsdom from 'jsdom';

const doc = jsdom.jsdom('<!doctype html><html><body></body></html>');
const win = doc.defaultView;

global.document = doc;
global.window = win;

Object.keys(window).forEach((key) => {
  if (!(key in global)) {
    global[key] = window[key];
  }
});

export const nextStateCalculate = {
	coins: [
		{index: 0, editing: false, face: 25, amount: 1},
		{index: 1, editing: false, face: 10, amount: 0},
		{index: 2, editing: false, face: 5, amount: 1},
		{index: 3, editing: false, face: 1, amount: 2}
	],
	value: 32
};

export const nextStateChangeCoin = {
	coins: [
		{index: 0, editing: false, face: 25, amount: 0},
		{index: 1, editing: false, face: 10, amount: 0},
		{index: 2, editing: false, face: 6, amount: 0},
		{index: 3, editing: false, face: 1, amount: 0}
	],
	value: 0
}

export const nextStateChainActions = {
	coins: [
		{index: 0, editing: false, face: 25, amount: 1},
		{index: 1, editing: false, face: 10, amount: 0},
		{index: 2, editing: false, face: 6, amount: 1},
		{index: 3, editing: false, face: 1, amount: 1}
	],
	value: 32
}
