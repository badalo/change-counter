import * as types from '../src/actions/types';
import { expect } from 'chai';
import { defaultState } from '../src/store/default';
import MintReducer from '../src/reducers/MintReducers';
import { nextStateCalculate, nextStateChangeCoin, nextStateChainActions } from './test_helper';

describe('Mint reducer', () => {
	it('Can chain actions', () => {
		const actions = [
			{type: types.EDIT_COIN, index: 2},
			{type: types.CHANGE_COIN, index: 2, value: 6},
			{type: types.UPDATE_VALUE, value: 32},
			{type: types.CALCULATE}
		];
		const nextState = actions.reduce(MintReducer, defaultState);

		expect(nextState).to.eql(nextStateChainActions);
	});

	it('Update value to be calculated', () => {
		const state = defaultState;
		const action = {type: types.UPDATE_VALUE, value: 12};
		const nextState = MintReducer(state, action);

		expect(nextState).to.have.property('value').and.to.equal(12);
	})

	it('Calculate coins', () => {
		const actions = [
			{type: types.UPDATE_VALUE, value: 32},
			{type: types.CALCULATE}];
		const nextState = actions.reduce(MintReducer, defaultState);

		expect(nextState).to.eql(nextStateCalculate);
	});

	it('Edit coin face', () => {
		const state = defaultState;
		const action = {type: types.EDIT_COIN, index: 2};
		const nextState = MintReducer(state, action);

		expect(nextState).to.have.deep.property('coins[2].editing').to.be.true;
	});

	it('Changes coin face', () => {
		const state = defaultState;
		const action = {type: types.CHANGE_COIN, index: 2, value: 6};
		const nextState = MintReducer(state, action);

		expect(nextState).to.eql(nextStateChangeCoin);
	});
});
